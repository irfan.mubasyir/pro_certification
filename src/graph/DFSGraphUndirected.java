package graph;

// DFS algorithm in Java

class DFSGraphUndirected {
    private LinkedList adjLists[];
    private boolean visited[];

    // Graph creation
    DFSGraphUndirected(int vertices) {
        adjLists = new LinkedList[vertices];
        visited = new boolean[vertices];

        for (int i = 0; i < vertices; i++)
            adjLists[i] = new LinkedList();
    }

    // Add edges
    void addEdge(int src, int dest) {
        adjLists[src].add(dest);
    }

    // DFS algorithm
    void DFS(int vertex) {
        visited[vertex] = true;
        System.out.print(vertex + " ");

        LinkedList linkedList = adjLists[vertex];
        Node currNode = linkedList.head;

        while  (currNode != null) {

            int adj = currNode.data;
            if (!visited[adj])
                DFS(adj);

            currNode = currNode.next;
        }
    }

    public static void main(String args[]) {
        DFSGraphUndirected g = new DFSGraphUndirected(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        System.out.println("Following is Depth First Traversal");

        g.DFS(2);
    }
}
