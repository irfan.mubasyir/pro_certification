package graph;

// DFS algorithm in Java

import data_structure.MyStack;

class TopologicalSort {
    private LinkedList adjLists[];
    private boolean visited[];
    private MyStack stack;
    private int V;

    // Graph creation
    TopologicalSort(int vertices) {
        V = vertices;
        adjLists = new LinkedList[vertices];
        visited = new boolean[vertices];

        for (int i = 0; i < vertices; i++)
            adjLists[i] = new LinkedList();


    }

    // Add edges
    void addEdge(int src, int dest) {
        adjLists[src].add(dest);
    }

    // DFS algorithm
    void DFS(int vertex, boolean[] visited, MyStack stack) {
        visited[vertex] = true;


        LinkedList linkedList = adjLists[vertex];
        Node currNode = linkedList.head;

        while  (currNode != null) {

            int adj = currNode.data;
            if (!visited[adj])
                DFS(adj, visited, stack);

            currNode = currNode.next;
        }

        stack.push(vertex);
    }

    void topologicalSort() {
        boolean visited[] = new boolean[V];
        for (int i = 0; i < V; i++)
            visited[i] = false;

        stack = new MyStack(V);

        // Call the recursive helper
        // function to store
        // Topological Sort starting
        // from all vertices one by one
        for (int i = 0; i < V; i++)
            if (visited[i] == false)
                DFS(i, visited, stack);

        // Print contents of stack
        while (stack.isEmpty() == false)
            System.out.print(stack.pop() + " ");
    }

    public static void main(String args[]) {
        TopologicalSort g = new TopologicalSort(6);

        /*
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        System.out.println("Following is Depth First Traversal");

        g.DFS(2);*/


        g.addEdge(5, 2);
        g.addEdge(5, 0);
        g.addEdge(4, 0);
        g.addEdge(4, 1);
        g.addEdge(2, 3);
        g.addEdge(3, 1);

        System.out.println("Following is a Topological sort of the given graph");
        // Function Call
        g.topologicalSort();
    }
}
