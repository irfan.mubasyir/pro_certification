package data_structure;// Java program to demonstrate implementation of our
// own hash table with chaining for collision detection

// A MyNode of chains
class MyNode<K, V> {
    K key;
    V value;

    // Reference to next MyNode
    MyNode<K, V> next;

    // Constructor
    public MyNode(K key, V value)
    {
        this.key = key;
        this.value = value;
    }
}

// Class to represent entire hash table
public class MyHashMap<K, V> {
    // bucketArray is used to store array of chains
    private MyArrayList bucketArray;

    // Current capacity of array list
    private int numBuckets;

    // Current size of array list
    private int size;

    // Constructor (Initializes capacity, size and
    // empty chains.
    public MyHashMap()
    {
        bucketArray = new MyArrayList();
        numBuckets = 10;
        size = 0;

        // Create empty chains
        for (int i = 0; i < numBuckets; i++)
            bucketArray.add(null);
    }

    public int size() { return size; }
    public boolean isEmpty() { return size() == 0; }

    // This implements hash function to find index
    // for a key
    private int getBucketIndex(K key)
    {
        int hashCode = key.hashCode();
        int index = hashCode % numBuckets;
        // key.hashCode() could be negative.
        index = index < 0 ? index * -1 : index;
        return index;
    }

    // Method to remove a given key
    public V remove(K key) throws Exception
    {
        // Apply hash function to find index for given key
        int bucketIndex = getBucketIndex(key);

        // Get head of chain
        MyNode<K, V> head = (MyNode<K, V>) bucketArray.get(bucketIndex);

        // Search for key in its chain
        MyNode<K, V> prev = null;
        while (head != null) {
            // If Key found
            if (head.key.equals(key))
                break;

            // Else keep moving in chain
            prev = head;
            head = head.next;
        }

        // If key was not there
        if (head == null)
            return null;

        // Reduce size
        size--;

        // Remove key
        if (prev != null)
            prev.next = head.next;
        else
            bucketArray.set(bucketIndex, head.next);

        return head.value;
    }

    // Returns value for a key
    public V get(K key) throws Exception
    {
        // Find head of chain for given key
        int bucketIndex = getBucketIndex(key);
        MyNode<K, V> head = ( MyNode<K, V>) bucketArray.get(bucketIndex);

        // Search key in chain
        while (head != null) {
            if (head.key.equals(key))
                return head.value;
            head = head.next;
        }

        // If key not found
        return null;
    }

    // Adds a key value pair to hash
    public void add(K key, V value) throws  Exception
    {
        // Find head of chain for given key
        int bucketIndex = getBucketIndex(key);
        MyNode<K, V> head = (MyNode<K, V>) bucketArray.get(bucketIndex);

        // Check if key is already present
        while (head != null) {
            if (head.key.equals(key)) {
                head.value = value;
                return;
            }
            head = head.next;
        }

        // Insert key in chain
        size++;
        head = (MyNode<K, V> ) bucketArray.get(bucketIndex);
        MyNode<K, V> newMyNode = new MyNode<K, V>(key, value);
        newMyNode.next = head;
        bucketArray.set(bucketIndex, newMyNode);

        // If load factor goes beyond threshold, then
        // double hash table size
        if ((1.0 * size) / numBuckets >= 0.7) {
            MyArrayList temp = bucketArray;
            bucketArray = new MyArrayList();
            numBuckets = 2 * numBuckets;
            size = 0;
            for (int i = 0; i < numBuckets; i++)
                bucketArray.add(null);

            for (int i=0; i < temp.getSize(); i++) {
                MyNode<K, V> headMyNode = (MyNode<K, V>) temp.get(i);
                while (headMyNode != null) {
                    add(headMyNode.key, headMyNode.value);
                    headMyNode = headMyNode.next;
                }
            }
        }
    }

    // Driver method to test Map class
    public static void main(String[] args) throws Exception
    {
        MyHashMap<String, Integer> map = new MyHashMap<>();
        map.add("this", 1);
        map.add("coder", 2);
        map.add("this", 3);
        map.add("hi", 4);
        System.out.println("MAP SIZE = " + map.size());
        System.out.println("REMOVE THIS = " + map.remove("this"));
        System.out.println("REMOVE THIS AGAIN = " + map.remove("this"));
        System.out.println("MAP SIZE = " + map.size());
        System.out.println("MAP IS_EMPTY = " +map.isEmpty());
    }
}
